def fi(n):
    if n==1 or n==2:
        return 1
    else:
        return fi(n-1)+fi(n-2)

print fi(1)
print fi(2)
print fi(3)
print fi(4)
print fi(5)
print fi(6)